#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include "simulator.h"

#define NR_TIMERS 8    /* number of timers; this should be greater than half the number of sequence numbers. */
#define MAX_QUEUE 1000 /* max number of buffered frames */
#define FRAME_SIZE (sizeof(frame))
#define BYTE 0377           /* byte mask */
#define UINT_MAX 0xFFFFFFFF /* maximum value of an unsigned 32-bit int */


#define DEADLOCK (3 * timeout_interval) /* defines what a deadlock is */
#define MANY 256                        /* big enough to clear pipe at the end */

/* Status variables used by the workers, M0 and M1. */
bigint ack_timer[NR_TIMERS];              /* ack timers */
unsigned int seqs[NR_TIMERS];             /* last sequence number sent per timer */
bigint lowest_timer;                      /* lowest of the timers */
bigint aux_timer;                         /* value of the auxiliary timer */
int network_layer_status;                 /* 0 is disabled, 1 is enabled */
unsigned int next_net_pkt;                /* seq of next network packet to fetch */
unsigned int last_pkt_given = 0xFFFFFFFF; /* seq of last pkt delivered*/
frame last_frame;                         /* arrive frames are kept here */
int offset;                               /* to prevent multiple timeouts on same tick*/
bigint tick;                              /* current time */
int retransmitting;                       /* flag that is set on a timeout */
int nseqs = NR_TIMERS;                    /* must be MAX_SEQ + 1 after startup */
unsigned int oldest_frame = NR_TIMERS;    /* tells which frame timed out */

char *tag[] = {"Data", "Ack ", "Nak "};

/* Statistics */
int data_sent;          /* number of data frames sent */
int data_retransmitted; /* number of data frames retransmitted */
int data_lost;          /* number of data frames lost */
int data_not_lost;      /* number of data frames not lost */
int good_data_recd;     /* number of data frames received */
int cksum_data_recd;    /* number of bad data frames received */

int acks_sent;       /* number of ack frames sent */
int acks_lost;       /* number of ack frames lost */
int acks_not_lost;   /* number of ack frames not lost */
int good_acks_recd;  /* number of ack frames received */
int cksum_acks_recd; /* number of bad ack frames received */

int payloads_accepted; /* number of pkts passed to network layer */
int timeouts;          /* number of timeouts */
int ack_timeouts;      /* number of ack timeouts */

/* Incoming frames are buffered here for later processing. */
frame queue[MAX_QUEUE];  /* buffered incoming frames */
frame *inp = &queue[0];  /* where to put the next frame */
frame *outp = &queue[0]; /* where to remove the next frame from */
int nframes = 0; /* number of queued frames */

bigint tick = 0;  /* the current time, measured in events */
bigint last_tick; /* when to stop the simulation */
int hanging[2];   /* # times a process has done nothing */
struct sigaction act, oact;


void set_up_pipes(void) {
    /* Create six pipes so main, M0 and M1 can communicate pairwise. */

    int fd[2];

    pipe(fd);
    r1 = fd[0];
    w1 = fd[1]; /* M0 to M1 for frames */
    pipe(fd);
    r2 = fd[0];
    w2 = fd[1]; /* M1 to M0 for frames */
    pipe(fd);
    r3 = fd[0];
    w3 = fd[1]; /* main to M0 for go-ahead */
    pipe(fd);
    r4 = fd[0];
    w4 = fd[1]; /* M0 to main to signal readiness */
    pipe(fd);
    r5 = fd[0];
    w5 = fd[1]; /* main to M1 for go-ahead */
    pipe(fd);
    r6 = fd[0];
    w6 = fd[1]; /* M1 to main to signal readiness */
}

void sim_error(char *s) {
    /* A simulator error has occurred. */

    int fd;

    printf("%s\n", s);
    fd = (id == 0 ? w4 : w6);
    write(fd, &zero, TICK_SIZE);
    exit(1);
}

void fork_off_workers(void) {
    if (fork() != 0) {
        /* This is the Parent.  It will become main, but first fork off M1. */
        if (fork() != 0) {
            /* This is main. */
            sigaction(SIGPIPE, &act, &oact);
            setvbuf(stdout, (char *) 0, _IONBF, (size_t) 0); /*don't buffer*/
            close(r1);
            close(w1);
            close(r2);
            close(w2);
            close(r3);
            close(w4);
            close(r5);
            close(w6);
            return;
        } else {
            /* This is the code for M1. Run protocol. */
            close(w1);
            close(r2);
            close(r3);
            close(w3);
            close(r4);
            close(w4);
            close(w5);
            close(r6);
            if (fcntl(r1, F_SETFL, O_NONBLOCK + O_ASYNC) < 0) /*JH*/
                sim_error("pipe initialization failed for M1");
            id = 1;    /* M1 gets id 1 */
            mrfd = r5; /* fd for reading time from main */
            mwfd = w6; /* fd for writing reply to main */
            prfd = r1; /* fd for reading frames from worker 0 */

            (*proc2)(); /* call the user-defined protocol function */
            return;
        }
    } else {
        /* This is the code for M0. Run protocol. */
        close(r1);
        close(w2);
        close(w3);
        close(r4);
        close(r5);
        close(w5);
        close(r6);
        close(w6);

        if (fcntl(r2, F_SETFL, O_NONBLOCK + O_ASYNC) < 0) /*JH*/
            sim_error("pipe initialization failed for M1");

        id = 0;    /* M0 gets id 0 */
        mrfd = r3; /* fd for reading time from main */
        mwfd = w4; /* fd for writing reply to main */
        prfd = r2; /* fd for reading frames from worker 1 */
        (*proc1)(); /* call the user-defined protocol function */
        return;
    }
}

void terminate(char *s) {
    /* End the simulation run by sending each worker a 32-bit zero command. */

    int n, k1, k2, res1[MANY], res2[MANY];

    for (n = 0; n < MANY; n++) {
        res1[n] = 0;
        res2[n] = 0;
    }
    write(w3, &zero, TICK_SIZE);
    write(w5, &zero, TICK_SIZE);
    sleep(4);

    /* Clean out the pipe.  The zero word indicates start of statistics. */
    k1 = 0;
    while (res1[k1] != 0)
        k1++;

    /* Clean out the other pipe and look for statistics. */
    k2 = 0;
    while (res1[k2] != 0)
        k2++;

    if (strlen(s) > 0) {
        printf("%s.  Time=%lu\n", s, tick / DELTA);
    }
    exit(1);
}


void init_frame(frame *s) {
    /* Fill in fields that that the simulator expects. Protocols may update
     * some of these fields. This filling is not strictly needed, but makes the
     * simulation trace look better, showing unused fields as zeros.
     */

    s->seq = 0;
    s->ack = 0;
    s->kind = (id == 0 ? data : ack);
    s->info.data[0] = 0;
    s->info.data[1] = 0;
    s->info.data[2] = 0;
    s->info.data[3] = 0;
}

void queue_frames(void) {
    /* See if there is room in the circular buffer queue[]. If so try to read as
     * much from the pipe as possible.
     * If inp is near the top of queue[], a single call here
     * may read a few frames into the top of queue[] and then some more starting
     * at queue[0].  This is done in two read operations.
     */

    int prfd, frct, k;
    frame *top;

    prfd = (id == 0 ? r2 : r1); /* which file descriptor is pipe on */

    /* How many frames can be read consecutively? */
    top = (outp <= inp ? &queue[MAX_QUEUE] : outp); /* how far can we rd?*/
    k = top - inp;                                  /* number of frames that can be read consecutively */
    // /**/ fprintf(flog, "XQF1 k=%d, nframes=%d\n", k, nframes);
    frct = read(prfd, inp, k * FRAME_SIZE);
    // /**/ fprintf(flog, "XQF2 k=%d, nframes=%d\n", k, nframes);
    if (frct < 0) {
        if (errno != EAGAIN)
            sim_error("error in reading the pipe 1");
    }
    if (frct > 0) {
        nframes = nframes + frct / FRAME_SIZE;
        // /**/ fprintf(flog, "XQF3 k=%d, nframes=%d\n", k, nframes);
        inp = inp + frct / FRAME_SIZE;
        if (inp == &queue[MAX_QUEUE])
            inp = queue;
        // /**/ if (nframes > 0)
        // print_queue();
        if (frct / FRAME_SIZE == k) /*are there residual frames to be read? */
        {
            k = outp - inp;
            frct = read(prfd, inp, k * FRAME_SIZE);
            if (frct < 0) {
                if (errno != EAGAIN)
                    sim_error("error in reading the pipe 2");
            }
            if (frct > 0) {
                nframes = nframes + frct / FRAME_SIZE;
                inp = inp + frct / FRAME_SIZE;
                if (frct / FRAME_SIZE == k)
                    sim_error("queue full");
            }
        }
    }
}

void recalc_timers(void) {
    /* Find the lowest timer */

    int i;
    bigint t = UINT_MAX;

    for (i = 0; i < NR_TIMERS; i++) {
        if (ack_timer[i] > 0 && ack_timer[i] < t)
            t = ack_timer[i];
    }
    lowest_timer = t;
}


int check_timers(void) {
    /* Check for possible timeout.  If found, reset the timer. */

    int i;

    /* See if a timeout event is even possible now. */
    if (lowest_timer == 0 || tick < lowest_timer)
        return (-1);

    /* A timeout event is possible.  Find the lowest timer. Note that it is
     * impossible for two frame timers to have the same value, so that when a
     * hit is found, it is the only possibility.  The use of the offset variable
     * guarantees that each successive timer set gets a higher value than the
     * previous one.
     */
    for (i = 0; i < NR_TIMERS; i++) {
        if (ack_timer[i] == lowest_timer) {
            ack_timer[i] = 0;       /* turn the timer off */
            recalc_timers();        /* find new lowest timer */
            oldest_frame = seqs[i]; /* timed out sequence number */
            return (i);
        }
    }
    printf("Impossible.  check_timers failed at %lu\n", lowest_timer);
    exit(1);
}

int check_ack_timer() {
    /* See if the ack timer has expired. */

    if (aux_timer > 0 && tick >= aux_timer) {
        aux_timer = 0;
        return (1);
    } else {
        return (0);
    }
}

unsigned int pktnum(packet *p) {
    /* Extract packet number from packet. */

    unsigned int num, b0, b1, b2, b3;

    b0 = p->data[0] & BYTE;
    b1 = p->data[1] & BYTE;
    b2 = p->data[2] & BYTE;
    b3 = p->data[3] & BYTE;
    num = (b0 << 24) | (b1 << 16) | (b2 << 8) | b3;
    return (num);
}

void fr(frame *f) {
    /* Print frame information for tracing. */

    printf("type=%s  seq=%u  ack=%u  payload=%d\n",
           tag[f->kind], f->seq, f->ack, pktnum(&f->info));
}

event_type frametype(void) {
    /* This function is called after it has been decided that a frame_arrival
     * event will occur.  The earliest frame is removed from queue[] and copied
     * to last_frame.  This copying is needed to avoid messing up the simulation
     * in the event that the protocol does not actually read the incoming frame.
     * For example, in protocols 2 and 3, the senders do not call
     * from_physical_layer() to collect the incoming frame. If frametype() did
     * not remove incoming frames from queue[], they never would be removed.
     * Of course, one could change sender2() and sender3() to have them call
     * from_physical_layer(), but doing it this way is more robust.
     *
     * This function determines (stochastically) whether the arrived frame is good
     * or bad (contains a checksum error).
     */

    int n, i;
    event_type event;

    /* Remove one frame from the queue. */
    last_frame = *outp; /* copy the first frame in the queue */
    outp++;
    if (outp == &queue[MAX_QUEUE])
        outp = queue;
    nframes--;

    /* Generate frames with checksum errors at random. */
    n = rand() & 01777;
    if (n < garbled) {
        /* Checksum error.*/
        event = cksum_err;
        if (last_frame.kind == data)
            cksum_data_recd++;
        if (last_frame.kind == ack)
            cksum_acks_recd++;
        i = 0;
    } else {
        event = frame_arrival;
        if (last_frame.kind == data)
            good_data_recd++;
        if (last_frame.kind == ack)
            good_acks_recd++;
        i = 1;
    }


    return (event);
}


int pick_event(void) {
    /* Pick a random event that is now possible for the process.
     * Note that the order in which the tests is made is critical, as it gives
     * priority to some events over others.  For example, for protocols 3 and 4
     * frames will be delivered before a timeout will be caused.  This is probably
     * a reasonable strategy, and more closely models how a real line works.
     */

    if (check_ack_timer() > 0)
        return (ack_timeout);
    if (nframes > 0)
        return ((int) frametype());
    if (network_layer_status)
        return (network_layer_ready);
    if (check_timers() >= 0)
        return (timeout); /* timer went off */
    return no_event;
}

void from_network_layer(packet *p) {
    /* Fetch a packet from the network layer for transmission on the channel. */

    p->data[0] = (next_net_pkt >> 24) & BYTE;
    p->data[1] = (next_net_pkt >> 16) & BYTE;
    p->data[2] = (next_net_pkt >> 8) & BYTE;
    p->data[3] = (next_net_pkt) & BYTE;
    next_net_pkt++;
}


void to_network_layer(packet *p) {
    /* Deliver information from an inbound frame to the network layer. A check is
     * made to see if the packet is in sequence.  If it is not, the simulation
     * is terminated with a "protocol error" message.
     */

    unsigned int num;

    num = pktnum(p);
    if (num != last_pkt_given + 1) {
        printf("Tick %lu. Proc %d got protocol error.  Packet delivered out of order.\n", tick / DELTA, id);
        printf("Expected payload %d but got payload %d\n", last_pkt_given + 1, num);
        exit(0);
    }
    last_pkt_given = num;
    payloads_accepted++;
}

void from_physical_layer(frame *r) {
    /* Copy the newly-arrived frame to the user. */
    *r = last_frame;
}


void print_statistics(void) {
    /* Display statistics. */

    int word[3];

    sleep(id + 1); /* let p0 and p1 sleep for different times */ /*jh*/
    printf("\nProceso %d:\n", id);
    printf("\tTotal de tramas de datos enviadas:  %9d\n", data_sent);
    printf("\tTramas de datos perdidas:           %9d\n", data_lost);
    printf("\tTrama de datos no perdidas:         %9d\n", data_not_lost);
    printf("\tTramas retransmitidas:              %9d\n", data_retransmitted);
    printf("\tACK buenas recibidas:               %9d\n", good_acks_recd);
    printf("\tACK malas recibidas:                %9d\n\n", cksum_acks_recd);

    printf("\tTrama de datos buenas recibidas:    %9d\n", good_data_recd);
    printf("\tTrama de datos malas recibidas:     %9d\n", cksum_data_recd);
    printf("\tPayloads aceptados:                 %9d\n", payloads_accepted);
    printf("\tTramas de ACK enviadas:             %9d\n", acks_sent);
    printf("\tTramas de ACK perdidas:             %9d\n", acks_lost);
    printf("\tTramas de ACK perdidas:             %9d\n", acks_not_lost);
    printf("\tTimeouts:                           %9d\n", timeouts);
    printf("\tACK timeouts:                       %9d\n", ack_timeouts);
    fflush(stdin);

    word[0] = 0;
    word[1] = payloads_accepted;
    word[2] = data_sent;
    write(mwfd, word, 3 * sizeof(int)); /* tell main we are done printing */
    sleep(1);
    exit(0);
}


void to_physical_layer(frame *s) {
    /* Pass the frame to the physical layer for writing on pipe 1 or 2.
     * However, this is where bad packets are discarded: they never get written.
     */

    int fd, got, k;

    /* The following statement is essential to later on determine the timed
     * out sequence number, e.g. in protocol 6. Keeping track of
     * this information is a bit tricky, since the call to start_timer()
     * does not tell what the sequence number is, just the buffer.  The
     * simulator keeps track of sequence numbers using the array seqs[],
     * which records the sequence number of each data frame sent, so on a
     * timeout, knowing the buffer number makes it possible to determine
     * the sequence number.
     */
    if (s->kind == data)
        seqs[s->seq % nseqs] = s->seq; /*JH*/

    if (s->kind == data)
        data_sent++;
    if (s->kind == ack)
        acks_sent++;
    if (retransmitting)
        data_retransmitted++;

    /* Bad transmissions (checksum errors) are simulated here. */
    k = rand() & 01777; /* 0 <= k <= about 1000 (really 1023) */
    if (k < pkt_loss) { /* simulate packet loss */
        if (s->kind == data)
            data_lost++; /* statistics gathering */
        if (s->kind == ack)
            acks_lost++; /* ditto */
        return;
    }
    if (s->kind == data)
        data_not_lost++; /* statistics gathering */
    if (s->kind == ack)
        acks_not_lost++; /* ditto */
    fd = (id == 0 ? w1 : w2);

    got = write(fd, s, FRAME_SIZE);
    if (got != FRAME_SIZE)
        print_statistics(); /* must be done */

        printf("Tick %lu. Proc %d sent frame: ", tick / DELTA, id);
        fr(s);
}

void start_timer(seq_nr k) {
    /* Start a timer for a data frame. */

    ack_timer[k % nseqs] = tick + timeout_interval + offset; /*JH*/
    offset++;
    recalc_timers(); /* figure out which timer is now lowest */
}

void stop_timer(seq_nr k) {
    /* Stop a data frame timer. */

    ack_timer[k % nseqs] = 0; /*JH*/
    recalc_timers();          /* figure out which timer is now lowest */
}


int parse_parameters(int argc, char *argv[], long *event, int *timeout_interval, int *pkt_loss, int *garbled) {
    /* Help function for protocol writers to parse first five command-line
     * parameters that the simulator needs.
     */

    if (argc < 5) {
        printf("Se necesitan 4 parametros para correr el programa\n");
        return (0);
    }
    *event = atol(argv[1]);
    if (*event < 0) {
        printf("El numero de simulaciones tiene que ser positiva\n");
        return (0);
    }
    *timeout_interval = atoi(argv[2]);
    if (*timeout_interval < 0) {
        printf("El timeout tiene que ser mayor a 0\n");
        return (0);
    }
    *pkt_loss = atoi(argv[3]); /* percent of sends that chuck pkt out */
    if (*pkt_loss < 0 || *pkt_loss > 99) {
        printf("El porcentaje de perdida de paquetes tiene que estar entre el 0 y 99%\n");
        return (0);
    }
    *garbled = atoi(argv[4]);
    if (*garbled < 0 || *garbled > 99) {
        printf("El cksum rate tiene que estar entre el 0 y 99%\n");
        return (0);
    }
    return (1);
}


void wait_for_event(event_type *event) {
    /* Wait_for_event reads the pipe from main to get the time.  Then it
     * fstat's the pipe from the other worker to see if any
     * frames are there.  If so, if collects them all in the queue array.
     * Once the pipe is empty, it makes a decision about what to do next.
     */

    bigint ct, word = OK;

    offset = 0;         /* prevents two timeouts at the same tick */
    retransmitting = 0; /* counts retransmissions */
    while (true) {
        queue_frames(); /* go get any newly arrived frames */
        if (write(mwfd, &word, TICK_SIZE) != TICK_SIZE)
            print_statistics();
        if (read(mrfd, &ct, TICK_SIZE) != TICK_SIZE)
            print_statistics();
        if (ct == 0)
            print_statistics();
        tick = ct; /* update time */


        /* Now pick event. */
        *event = pick_event();
        if (*event == no_event) {
            word = (lowest_timer == 0 ? NOTHING : OK);
            continue;
        }
        word = OK;
        if (*event == timeout) {
            timeouts++;
            retransmitting = 1; /* enter retransmission mode */
            printf("Tick %lu. Proc %d got timeout for frame %d\n", tick / DELTA, id, oldest_frame);
        }

        if (*event == ack_timeout) {
            ack_timeouts++;
            printf("Tick %lu. Proc %d got ack timeout\n", tick / DELTA, id);
        }
        return;
    }
}


void start_simulator(void (*p1)(), void (*p2)(), long event, int tm_out, int pk_loss, int grb) {
    /* El simulador tiene tres procesos: main(este proceso), M0 y M1
     * main mantiene el reloj (tick), y elige un proceso para correr.  Para estro escribe una palabra de 32 bits.
     * El proceso devuelve una respuesta cuando termino.
     * Luego Main seleccion otro proceso y se repite el ciclo
     */

    int process = 0; /* whose turn is it */
    int rfd, wfd;    /* file descriptor for talking to workers */
    bigint word;     /* message from worker */

    proc1 = p1;
    proc2 = p2;
    /* Each event uses DELTA ticks to make it possible for each timeout to
     * occur at a different tick.  For example, with DELTA = 10, ticks will
     * occur at 0, 10, 20, etc.  This makes it possible to schedule multiple
     * events (e.g. in protocol 5), all at unique times, e.g. 1070, 1071, 1072,
     * 1073, etc.  This property is needed to make sure timers go off in the
     * order they were set.  As a consequence, internally, the variable tick
     * is bumped by DELTA on each event. Thus asking for a simulation run of
     * 1000 events will give 1000 events, but they internally they will be
     * called 0 to 10,000.
     */
    last_tick = DELTA * event;

    /* Convert from external units to internal units so the user does not see
     * the internal units at all.
     */
    timeout_interval = DELTA * tm_out;

    /* Packet loss takes place at the sender.  Packets selected for being lost
     * are not put on the wire at all.  Internally, pkt_loss and garbled are
     * from 0 to 990 so they can be compared to 10 bit random numbers.  The
     * inaccuracy here is about 2.4% because 1000 != 1024.  In effect, it is
     * not possible to say that all packets are lost.  The most that can be
     * be lost is 990/1024.
     */

    pkt_loss = 10 * pk_loss; /* for our purposes, 1000 == 1024 */

    /* This arg tells what fraction of arriving packets are garbled.  Thus if
     * pkt_loss is 50 and garbled is 50, half of all packets (actually,
     * 500/1024 of all packets) will not be sent at all, and of the ones that
     * are sent, 500/1024 will arrive garbled.
     */

    garbled = 10 * grb; /* for our purposes, 1000 == 1024 */

    /* Turn tracing options on or off.  The bits are defined in worker.c. */
    printf("\n\nEventos: %lu    Parametros: %lu %d %u\n",
           last_tick / DELTA, timeout_interval / DELTA, pkt_loss / 10, garbled / 10);

    set_up_pipes();     /* create five pipes */
    fork_off_workers(); /* fork off the worker processes */

    /* Main simulation loop. */
    while (tick < last_tick) {
        process = rand() & 1; /* pick process to run: 0 or 1 */
        tick = tick + DELTA;
        rfd = (process == 0 ? r4 : r6);
        if (read(rfd, &word, TICK_SIZE) != TICK_SIZE)
            terminate("");
        if (word == OK)
            hanging[process] = 0;
        if (word == NOTHING)
            hanging[process] += DELTA;
        if (hanging[0] >= DEADLOCK && hanging[1] >= DEADLOCK)
            terminate("A deadlock has been detected");

        /* Write the time to the selected process to tell it to run. */
        wfd = (process == 0 ? w3 : w5);
        if (write(wfd, &tick, TICK_SIZE) != TICK_SIZE)
            terminate("Main could not write to worker");
    }
    /* Simulation run has finished. */
    terminate("End of simulation");
}
