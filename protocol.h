#include <stdio.h>
#include <stdlib.h>

#define MAX_PKT 4 /* determina el tamaño del paquete en bytes */
typedef enum {
    false,
    true
} boolean;
typedef unsigned int seq_nr; /* Numeros de secuencia o confirmacion */
typedef struct {
    unsigned char data[MAX_PKT];
} packet; /* Declaracion de paquete de la capa de red */
typedef enum {
    data,
    ack,
} frame_kind; /* definimos el tipo de trama */
typedef struct { /* definicion de la trama */
    frame_kind kind;
    seq_nr seq;
    seq_nr ack;  /* Confirmacion de recepcion */
    packet info; /*Paquete proveniente de la capa de red*/
} frame;

/*
Descripcion de parametros de start_simulator:
    proc1: El nombre de la funcion a ejecutar en la maquina 0.
    proc2: El nombre de la funcion a ejecutar en la maquina 1.
    event: indica cuánto tiempo se debe ejecutar la simulación.
    tm_out: Da el intervalo de tiempo de timeout, un intervalo pequeño daña considerablemente el rendimiento del protocolo.
    pk_loss: Da el porcentaje de tramas que se pierden (0-99)
    grb: Da el porcentaje de tramas que llegan que son malas (0-99), debido a errores en el checksum.
 */
void start_simulator(void (*proc1)(), void (*proc2)(), long event,int tm_out, int pk_loss, int grb);
/* init_frame llena los valores por defecto de la trama.
 * No es completamente necesario pero hace el seguimiento de la simulacion mejor, mostrando los campos que no se usan en 0
 */
void init_frame(frame *s);
/* Espera a que ocurra un evento */
void wait_for_event(event_type *event);
/* Obtener un paquete de la capa de red (Lo usa la maquina A) */
void from_network_layer(packet *p);
/* Entrega informacion de la trama entrante a la capa de red. (Lo usa la maquina B) */
void to_network_layer(packet *p);
/* Obtiene una trama de la capa fisica (Maquina B) */
void from_physical_layer(frame *r);
/* Envia la trama a la capa fisica para transmitirla (Maquina A)*/
void to_physical_layer(frame *s);
/* Arranca el temporizador y habilita el evento de expiracion de temporizador */
void start_timer(seq_nr k);
/* Detiene el temporizador e inhabilita el evento de expiracion de temporizador */
void stop_timer(seq_nr k);

/* In case of a timeout event, it is possible to find out the sequence
 * number of the frame that timed out (this is the sequence number parameter
 * in the start_timer function). For this, the simulator must know the maximum possible value that a sequence number may have. Function init_max_seqnr()
 * tells the simulator this value. This function must be called before
 * start_simulator() function. When a timeout event occurs, function
 * get_timedout_seqnr() returns the sequence number of the frame that timed out.
 */

int parse_parameters(int argc, char *argv[], long *event, int *timeout_interval, int *pkt_loss, int *garbled);
#define inc(k) if (k < 1) k += 1; else k = 0