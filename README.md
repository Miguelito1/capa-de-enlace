# Programacion capa de enlace UTN FRM

    gcc -c simulator.c

El comando anterior crea un objeto llamada simulator.o

Para compilar el protocolo ejecutamos el siguiente comando

    gcc -o programa protocol.c simulator.o

Para su ejecucion el programa toma las siguientes opciones:

    programa evento  timeout  pct_loss  pct_cksum

Donde

        evento    Indica cuánto tiempo se debe ejecutar la simulación.
        timeout   Da el intervalo de tiempo del timeout
        pct_loss  Da el porcentaje de tramas que se pierden (0-99 %)
        pct_cksum Da el porcentaje de tramas que llegan que son malas (0-99), debido a errores en el checksum.

Ejemplo

    ./programa 100000 40 20 10
