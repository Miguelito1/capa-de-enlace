typedef enum {
    frame_arrival, cksum_err, timeout
} event_type;

#include <stdio.h>
#include <stdlib.h>
#include "protocol.h"

void sender(void) {
    seq_nr next_frame_to_send; /* numero de secuencia para la siguiente trama */
    frame s;                   /* scratch variable */
    packet buffer;             /* buffer para el paquete de salida */
    event_type event;

    next_frame_to_send = 0;      /* inicializa los numeros de secuencia */
    from_network_layer(&buffer); /* obtener el paquete de la capa de enlace */
    while (true) {
        init_frame(&s);
        s.info = buffer;            /* inserta lo que recibimos de la capa de red a la trama */
        s.seq = next_frame_to_send; /* colocamos el numero de secuencia a la trama */
        to_physical_layer(&s);      /* manda la trama a la pc b por la capa fisica */
        start_timer(s.seq);         /* si la respuesta tarda mucho, expirar el temporizador */
        wait_for_event(&event);     /* Espera por evento del tipo frame_arrival, cksum_err, timeout */
        if (event == frame_arrival) {
            from_physical_layer(&s); /* recibe la trama */
            if (s.ack == next_frame_to_send) {
                stop_timer(s.ack);
                from_network_layer(&buffer); /* obtiene la siguiente trama a enviar */
                inc(next_frame_to_send);     /* incrementar (invertir) el valor de next_frame_to_send */
            }
        }
    }
}

void receiver(void) {
    seq_nr frame_expected;
    frame r, s;
    event_type event;
    frame_expected = 0;
    while (true) {
        wait_for_event(&event); /* posibilidades: frame_arrival, cksum_err */
        if (event == frame_arrival) {
            /* La trama es valida */
            from_physical_layer(&r); /* obtener la trama que recien llego */
            if (r.seq == frame_expected) {
                /* This is what we have been waiting for. */
                to_network_layer(&r.info); /* pasa los datos a la capa de red */
                inc(frame_expected);       /* incrementar (invertir) el valor de frame_expected */
            }
            init_frame(&s);
            s.ack = 1 - frame_expected; /* indica la trama cuya recepcion se esta confirmando */
            to_physical_layer(&s);      /* manda la trama con la confirmacion de recepcion */
        }
    }
}

int main(int argc, char *argv[]) {
    int timeout_interval, pkt_loss, garbled, debug_flags;
    long event;

    if (!parse_parameters(argc, argv, &event, &timeout_interval, &pkt_loss, &garbled)) {
        printf("Usage: p3 events timeout loss cksum debug\n");
        exit(1);
    }
    printf("\n\n Arrancando la simulacion 3\n");
    start_simulator(sender, receiver, event, timeout_interval, pkt_loss, garbled);
}
